package cat.epiaedu.damviod.pmdm;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

import static com.badlogic.gdx.Gdx.audio;

public class GameTest implements ApplicationListener, InputProcessor {
	SpriteBatch batch;
	Texture img;
	private BitmapFont font;
    private float elapsedTime=0;
	Sound sound;
    TextureAtlas textureAtlas;
    Animation animation;
	float velocitat =0;
	public Rectangle sprite;
	TextureRegion texture;
	float speed;
	boolean flip=false;
	Sound soundRunning;
	long soundID;
	boolean on=false;
	long soundIDD;
	float pitch;
	float velocity=0;
	@Override
	public void create () {
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");
		Gdx.app.log("Veocitat: ", "onCreate");
        font = new BitmapFont();
        font.setColor(Color.BLUE);
		Gdx.input.setInputProcessor(this);
        textureAtlas = new TextureAtlas(Gdx.files.internal("atlas/pack.atlas"));
		sprite=new Rectangle();
        animation = new Animation(1/15f, textureAtlas.getRegions());
		sprite.x=230;
		sprite.y=120;
		speed=0;
		texture=new TextureRegion();
		sound = Gdx.audio.newSound(Gdx.files.internal("sounds/engine-idle.wav"));
		soundRunning = Gdx.audio.newSound(Gdx.files.internal("sounds/engine-running.wav"));
		soundID=sound.play();
		sound.setLooping(soundID,true);

	}

	@Override
	public void resize(int width, int height) {
		Gdx.app.log("TestX", "onResize");
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();
		sprite.x+=speed;
		/*Color previousColor=batch.getColor();
		batch.draw(img, 0, 0);
		batch.draw(img, 200, 400);
		batch.setColor(0, 255,0,1);
		batch.draw(img, 200, 300);
        batch.setColor(previousColor);*/
        elapsedTime += Gdx.graphics.getDeltaTime();
		texture.setRegion((TextureRegion) animation.getKeyFrame(elapsedTime, true));
		if(on==true)
		{
			velocity+=5.0f*elapsedTime;
			velocitat+=5.0f;
		}
		if(!on)
		{
			velocity-=5.0f*elapsedTime;
			velocitat-=5.0f;
			if(velocity<=0)
			{
				velocity=0;
			}
			if(velocitat<=0)
			{
				velocitat=0;
			}
		}
		if(velocitat>=200)
		{
			velocitat=200;
		}
		if(velocity>=300)
		{
			velocity=300;
		}
		pitch =0.5f+velocity/200*0.5f;
		soundRunning.setPitch(soundIDD,pitch);

		if(flip)
		{
			texture.flip(true,false);
		}
		else
		{
			texture.flip(false,false);
		}
        batch.draw(texture, sprite.x, sprite.y);
        font.draw(batch, "Velocitat: "+velocitat+" km", 270, 200);
		batch.end();

	}

	@Override
	public void pause() {
		Gdx.app.log("TestX", "onPause");
	}

	@Override
	public void resume() {
		Gdx.app.log("TestX", "onResume");
	}

	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
        textureAtlas.dispose();
		Gdx.app.log("TestX", "onDispose");
	}

	@Override
	public boolean keyDown(int keycode) {
		Float x = Gdx.input.getX() - sprite.y/2;
		Float y = Gdx.graphics.getHeight() - Gdx.input.getY() - sprite.x/2;

		if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
				sprite.y=y;
				sprite.x=x;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){

			flip=true;
			speed-=5;
		}

		if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
			flip=false;
			speed+=5;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.SPACE))
		{
			sound.stop();
			soundIDD=soundRunning.play();
			on=true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		speed=0;
		on=false;
		return false;
	}

	@Override
	public boolean keyTyped(char character) {


		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		Integer x = Gdx.input.getX() - texture.getRegionWidth()/2;
		Integer y = (Gdx.graphics.getHeight() - Gdx.input.getY()) - texture.getRegionHeight()/2;
		if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
			sprite.y=y;
			sprite.x=x;
		}

		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
